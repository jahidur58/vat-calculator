// Generated code from Butter Knife. Do not modify!
package lt.ito.vatconverter.ui.vat;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;
import lt.ito.vatconverter.R;

public class VatActivity_ViewBinding implements Unbinder {
  private VatActivity target;

  @UiThread
  public VatActivity_ViewBinding(VatActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public VatActivity_ViewBinding(VatActivity target, View source) {
    this.target = target;

    target.mToolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'mToolbar'", Toolbar.class);
    target.mSpinnerCountry = Utils.findRequiredViewAsType(source, R.id.spinner_country, "field 'mSpinnerCountry'", Spinner.class);
    target.mRadioGroup = Utils.findRequiredViewAsType(source, R.id.radio_group, "field 'mRadioGroup'", RadioGroup.class);
    target.mEditTextAmount = Utils.findRequiredViewAsType(source, R.id.edit_text_amount, "field 'mEditTextAmount'", EditText.class);
    target.mTextViewResult = Utils.findRequiredViewAsType(source, R.id.text_view_result_amount, "field 'mTextViewResult'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    VatActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mToolbar = null;
    target.mSpinnerCountry = null;
    target.mRadioGroup = null;
    target.mEditTextAmount = null;
    target.mTextViewResult = null;
  }
}
