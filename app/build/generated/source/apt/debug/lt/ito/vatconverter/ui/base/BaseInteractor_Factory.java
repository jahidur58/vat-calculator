package lt.ito.vatconverter.ui.base;

import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;
import lt.ito.vatconverter.data.network.ApiHelper;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class BaseInteractor_Factory implements Factory<BaseInteractor> {
  private final Provider<ApiHelper> apiHelperProvider;

  public BaseInteractor_Factory(Provider<ApiHelper> apiHelperProvider) {
    assert apiHelperProvider != null;
    this.apiHelperProvider = apiHelperProvider;
  }

  @Override
  public BaseInteractor get() {
    return new BaseInteractor(apiHelperProvider.get());
  }

  public static Factory<BaseInteractor> create(Provider<ApiHelper> apiHelperProvider) {
    return new BaseInteractor_Factory(apiHelperProvider);
  }
}
