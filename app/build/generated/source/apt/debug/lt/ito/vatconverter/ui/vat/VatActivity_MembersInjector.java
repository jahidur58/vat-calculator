package lt.ito.vatconverter.ui.vat;

import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class VatActivity_MembersInjector implements MembersInjector<VatActivity> {
  private final Provider<VatPresenter<VatView, VatInteractor>> mPresenterProvider;

  public VatActivity_MembersInjector(
      Provider<VatPresenter<VatView, VatInteractor>> mPresenterProvider) {
    assert mPresenterProvider != null;
    this.mPresenterProvider = mPresenterProvider;
  }

  public static MembersInjector<VatActivity> create(
      Provider<VatPresenter<VatView, VatInteractor>> mPresenterProvider) {
    return new VatActivity_MembersInjector(mPresenterProvider);
  }

  @Override
  public void injectMembers(VatActivity instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.mPresenter = mPresenterProvider.get();
  }

  public static void injectMPresenter(
      VatActivity instance, Provider<VatPresenter<VatView, VatInteractor>> mPresenterProvider) {
    instance.mPresenter = mPresenterProvider.get();
  }
}
