package lt.ito.vatconverter.di.module;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;
import lt.ito.vatconverter.ui.vat.VatInteractor;
import lt.ito.vatconverter.ui.vat.VatInteractorImpl;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ActivityModule_ProvideVatInteractorFactory implements Factory<VatInteractor> {
  private final ActivityModule module;

  private final Provider<VatInteractorImpl> interactorProvider;

  public ActivityModule_ProvideVatInteractorFactory(
      ActivityModule module, Provider<VatInteractorImpl> interactorProvider) {
    assert module != null;
    this.module = module;
    assert interactorProvider != null;
    this.interactorProvider = interactorProvider;
  }

  @Override
  public VatInteractor get() {
    return Preconditions.checkNotNull(
        module.provideVatInteractor(interactorProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<VatInteractor> create(
      ActivityModule module, Provider<VatInteractorImpl> interactorProvider) {
    return new ActivityModule_ProvideVatInteractorFactory(module, interactorProvider);
  }

  /** Proxies {@link ActivityModule#provideVatInteractor(VatInteractorImpl)}. */
  public static VatInteractor proxyProvideVatInteractor(
      ActivityModule instance, VatInteractorImpl interactor) {
    return instance.provideVatInteractor(interactor);
  }
}
