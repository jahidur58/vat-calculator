package lt.ito.vatconverter.ui.vat;

import dagger.MembersInjector;
import dagger.internal.Factory;
import dagger.internal.MembersInjectors;
import javax.annotation.Generated;
import javax.inject.Provider;
import lt.ito.vatconverter.data.network.ApiHelper;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class VatInteractorImpl_Factory implements Factory<VatInteractorImpl> {
  private final MembersInjector<VatInteractorImpl> vatInteractorImplMembersInjector;

  private final Provider<ApiHelper> apiHelperProvider;

  public VatInteractorImpl_Factory(
      MembersInjector<VatInteractorImpl> vatInteractorImplMembersInjector,
      Provider<ApiHelper> apiHelperProvider) {
    assert vatInteractorImplMembersInjector != null;
    this.vatInteractorImplMembersInjector = vatInteractorImplMembersInjector;
    assert apiHelperProvider != null;
    this.apiHelperProvider = apiHelperProvider;
  }

  @Override
  public VatInteractorImpl get() {
    return MembersInjectors.injectMembers(
        vatInteractorImplMembersInjector, new VatInteractorImpl(apiHelperProvider.get()));
  }

  public static Factory<VatInteractorImpl> create(
      MembersInjector<VatInteractorImpl> vatInteractorImplMembersInjector,
      Provider<ApiHelper> apiHelperProvider) {
    return new VatInteractorImpl_Factory(vatInteractorImplMembersInjector, apiHelperProvider);
  }
}
