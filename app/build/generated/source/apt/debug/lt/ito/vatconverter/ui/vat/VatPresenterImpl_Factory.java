package lt.ito.vatconverter.ui.vat;

import dagger.MembersInjector;
import dagger.internal.Factory;
import dagger.internal.MembersInjectors;
import io.reactivex.disposables.CompositeDisposable;
import javax.annotation.Generated;
import javax.inject.Provider;
import lt.ito.vatconverter.utils.rx.SchedulerProvider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class VatPresenterImpl_Factory<V extends VatView, I extends VatInteractor>
    implements Factory<VatPresenterImpl<V, I>> {
  private final MembersInjector<VatPresenterImpl<V, I>> vatPresenterImplMembersInjector;

  private final Provider<I> mvpInteractorProvider;

  private final Provider<SchedulerProvider> schedulerProvider;

  private final Provider<CompositeDisposable> compositeDisposableProvider;

  public VatPresenterImpl_Factory(
      MembersInjector<VatPresenterImpl<V, I>> vatPresenterImplMembersInjector,
      Provider<I> mvpInteractorProvider,
      Provider<SchedulerProvider> schedulerProvider,
      Provider<CompositeDisposable> compositeDisposableProvider) {
    assert vatPresenterImplMembersInjector != null;
    this.vatPresenterImplMembersInjector = vatPresenterImplMembersInjector;
    assert mvpInteractorProvider != null;
    this.mvpInteractorProvider = mvpInteractorProvider;
    assert schedulerProvider != null;
    this.schedulerProvider = schedulerProvider;
    assert compositeDisposableProvider != null;
    this.compositeDisposableProvider = compositeDisposableProvider;
  }

  @Override
  public VatPresenterImpl<V, I> get() {
    return MembersInjectors.injectMembers(
        vatPresenterImplMembersInjector,
        new VatPresenterImpl<V, I>(
            mvpInteractorProvider.get(),
            schedulerProvider.get(),
            compositeDisposableProvider.get()));
  }

  public static <V extends VatView, I extends VatInteractor> Factory<VatPresenterImpl<V, I>> create(
      MembersInjector<VatPresenterImpl<V, I>> vatPresenterImplMembersInjector,
      Provider<I> mvpInteractorProvider,
      Provider<SchedulerProvider> schedulerProvider,
      Provider<CompositeDisposable> compositeDisposableProvider) {
    return new VatPresenterImpl_Factory<V, I>(
        vatPresenterImplMembersInjector,
        mvpInteractorProvider,
        schedulerProvider,
        compositeDisposableProvider);
  }
}
