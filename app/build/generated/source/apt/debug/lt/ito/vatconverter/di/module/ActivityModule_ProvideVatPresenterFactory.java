package lt.ito.vatconverter.di.module;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;
import lt.ito.vatconverter.ui.vat.VatInteractor;
import lt.ito.vatconverter.ui.vat.VatPresenter;
import lt.ito.vatconverter.ui.vat.VatPresenterImpl;
import lt.ito.vatconverter.ui.vat.VatView;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ActivityModule_ProvideVatPresenterFactory
    implements Factory<VatPresenter<VatView, VatInteractor>> {
  private final ActivityModule module;

  private final Provider<VatPresenterImpl<VatView, VatInteractor>> presenterProvider;

  public ActivityModule_ProvideVatPresenterFactory(
      ActivityModule module, Provider<VatPresenterImpl<VatView, VatInteractor>> presenterProvider) {
    assert module != null;
    this.module = module;
    assert presenterProvider != null;
    this.presenterProvider = presenterProvider;
  }

  @Override
  public VatPresenter<VatView, VatInteractor> get() {
    return Preconditions.checkNotNull(
        module.provideVatPresenter(presenterProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<VatPresenter<VatView, VatInteractor>> create(
      ActivityModule module, Provider<VatPresenterImpl<VatView, VatInteractor>> presenterProvider) {
    return new ActivityModule_ProvideVatPresenterFactory(module, presenterProvider);
  }

  /** Proxies {@link ActivityModule#provideVatPresenter(VatPresenterImpl)}. */
  public static VatPresenter<VatView, VatInteractor> proxyProvideVatPresenter(
      ActivityModule instance, VatPresenterImpl<VatView, VatInteractor> presenter) {
    return instance.provideVatPresenter(presenter);
  }
}
