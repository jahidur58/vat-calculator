package lt.ito.vatconverter.di.component;

import dagger.MembersInjector;
import dagger.internal.DoubleCheck;
import dagger.internal.Factory;
import dagger.internal.MembersInjectors;
import dagger.internal.Preconditions;
import io.reactivex.disposables.CompositeDisposable;
import javax.annotation.Generated;
import javax.inject.Provider;
import lt.ito.vatconverter.data.network.ApiHelper;
import lt.ito.vatconverter.di.module.ActivityModule;
import lt.ito.vatconverter.di.module.ActivityModule_ProvideCompositeDisposableFactory;
import lt.ito.vatconverter.di.module.ActivityModule_ProvideSchedulerProviderFactory;
import lt.ito.vatconverter.di.module.ActivityModule_ProvideVatInteractorFactory;
import lt.ito.vatconverter.di.module.ActivityModule_ProvideVatPresenterFactory;
import lt.ito.vatconverter.ui.vat.VatActivity;
import lt.ito.vatconverter.ui.vat.VatActivity_MembersInjector;
import lt.ito.vatconverter.ui.vat.VatInteractor;
import lt.ito.vatconverter.ui.vat.VatInteractorImpl;
import lt.ito.vatconverter.ui.vat.VatInteractorImpl_Factory;
import lt.ito.vatconverter.ui.vat.VatPresenter;
import lt.ito.vatconverter.ui.vat.VatPresenterImpl;
import lt.ito.vatconverter.ui.vat.VatPresenterImpl_Factory;
import lt.ito.vatconverter.ui.vat.VatView;
import lt.ito.vatconverter.utils.rx.SchedulerProvider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerActivityComponent implements ActivityComponent {
  private Provider<ApiHelper> apiHelperProvider;

  private Provider<VatInteractorImpl> vatInteractorImplProvider;

  private Provider<VatInteractor> provideVatInteractorProvider;

  private Provider<SchedulerProvider> provideSchedulerProvider;

  private Provider<CompositeDisposable> provideCompositeDisposableProvider;

  private Provider<VatPresenterImpl<VatView, VatInteractor>> vatPresenterImplProvider;

  private Provider<VatPresenter<VatView, VatInteractor>> provideVatPresenterProvider;

  private MembersInjector<VatActivity> vatActivityMembersInjector;

  private DaggerActivityComponent(Builder builder) {
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {

    this.apiHelperProvider =
        new Factory<ApiHelper>() {
          private final ApplicationComponent applicationComponent = builder.applicationComponent;

          @Override
          public ApiHelper get() {
            return Preconditions.checkNotNull(
                applicationComponent.apiHelper(),
                "Cannot return null from a non-@Nullable component method");
          }
        };

    this.vatInteractorImplProvider =
        VatInteractorImpl_Factory.create(
            MembersInjectors.<VatInteractorImpl>noOp(), apiHelperProvider);

    this.provideVatInteractorProvider =
        DoubleCheck.provider(
            ActivityModule_ProvideVatInteractorFactory.create(
                builder.activityModule, vatInteractorImplProvider));

    this.provideSchedulerProvider =
        ActivityModule_ProvideSchedulerProviderFactory.create(builder.activityModule);

    this.provideCompositeDisposableProvider =
        ActivityModule_ProvideCompositeDisposableFactory.create(builder.activityModule);

    this.vatPresenterImplProvider =
        VatPresenterImpl_Factory.create(
            MembersInjectors.<VatPresenterImpl<VatView, VatInteractor>>noOp(),
            provideVatInteractorProvider,
            provideSchedulerProvider,
            provideCompositeDisposableProvider);

    this.provideVatPresenterProvider =
        DoubleCheck.provider(
            ActivityModule_ProvideVatPresenterFactory.create(
                builder.activityModule, vatPresenterImplProvider));

    this.vatActivityMembersInjector =
        VatActivity_MembersInjector.create(provideVatPresenterProvider);
  }

  @Override
  public void inject(VatActivity activity) {
    vatActivityMembersInjector.injectMembers(activity);
  }

  public static final class Builder {
    private ActivityModule activityModule;

    private ApplicationComponent applicationComponent;

    private Builder() {}

    public ActivityComponent build() {
      if (activityModule == null) {
        throw new IllegalStateException(ActivityModule.class.getCanonicalName() + " must be set");
      }
      if (applicationComponent == null) {
        throw new IllegalStateException(
            ApplicationComponent.class.getCanonicalName() + " must be set");
      }
      return new DaggerActivityComponent(this);
    }

    public Builder activityModule(ActivityModule activityModule) {
      this.activityModule = Preconditions.checkNotNull(activityModule);
      return this;
    }

    public Builder applicationComponent(ApplicationComponent applicationComponent) {
      this.applicationComponent = Preconditions.checkNotNull(applicationComponent);
      return this;
    }
  }
}
