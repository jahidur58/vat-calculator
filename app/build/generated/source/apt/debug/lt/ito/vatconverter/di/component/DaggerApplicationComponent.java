package lt.ito.vatconverter.di.component;

import android.app.Application;
import android.content.Context;
import dagger.MembersInjector;
import dagger.internal.DoubleCheck;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;
import lt.ito.vatconverter.MvpApp;
import lt.ito.vatconverter.MvpApp_MembersInjector;
import lt.ito.vatconverter.data.network.ApiHelper;
import lt.ito.vatconverter.data.network.AppApiHelper;
import lt.ito.vatconverter.data.network.AppApiHelper_Factory;
import lt.ito.vatconverter.di.module.ApplicationModule;
import lt.ito.vatconverter.di.module.ApplicationModule_ProvideApiHelperFactory;
import lt.ito.vatconverter.di.module.ApplicationModule_ProvideApplicationFactory;
import lt.ito.vatconverter.di.module.ApplicationModule_ProvideCalligraphyDefaultConfigFactory;
import lt.ito.vatconverter.di.module.ApplicationModule_ProvideContextFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerApplicationComponent implements ApplicationComponent {
  private Provider<CalligraphyConfig> provideCalligraphyDefaultConfigProvider;

  private MembersInjector<MvpApp> mvpAppMembersInjector;

  private Provider<Context> provideContextProvider;

  private Provider<Application> provideApplicationProvider;

  private Provider<AppApiHelper> appApiHelperProvider;

  private Provider<ApiHelper> provideApiHelperProvider;

  private DaggerApplicationComponent(Builder builder) {
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {

    this.provideCalligraphyDefaultConfigProvider =
        DoubleCheck.provider(
            ApplicationModule_ProvideCalligraphyDefaultConfigFactory.create(
                builder.applicationModule));

    this.mvpAppMembersInjector =
        MvpApp_MembersInjector.create(provideCalligraphyDefaultConfigProvider);

    this.provideContextProvider =
        ApplicationModule_ProvideContextFactory.create(builder.applicationModule);

    this.provideApplicationProvider =
        ApplicationModule_ProvideApplicationFactory.create(builder.applicationModule);

    this.appApiHelperProvider = DoubleCheck.provider(AppApiHelper_Factory.create());

    this.provideApiHelperProvider =
        DoubleCheck.provider(
            ApplicationModule_ProvideApiHelperFactory.create(
                builder.applicationModule, appApiHelperProvider));
  }

  @Override
  public void inject(MvpApp app) {
    mvpAppMembersInjector.injectMembers(app);
  }

  @Override
  public Context context() {
    return provideContextProvider.get();
  }

  @Override
  public Application application() {
    return provideApplicationProvider.get();
  }

  @Override
  public ApiHelper apiHelper() {
    return provideApiHelperProvider.get();
  }

  public static final class Builder {
    private ApplicationModule applicationModule;

    private Builder() {}

    public ApplicationComponent build() {
      if (applicationModule == null) {
        throw new IllegalStateException(
            ApplicationModule.class.getCanonicalName() + " must be set");
      }
      return new DaggerApplicationComponent(this);
    }

    public Builder applicationModule(ApplicationModule applicationModule) {
      this.applicationModule = Preconditions.checkNotNull(applicationModule);
      return this;
    }
  }
}
