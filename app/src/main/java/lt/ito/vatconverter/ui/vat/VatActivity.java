package lt.ito.vatconverter.ui.vat;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import lt.ito.vatconverter.R;
import lt.ito.vatconverter.data.network.model.Period;
import lt.ito.vatconverter.data.network.model.Rate;
import lt.ito.vatconverter.ui.base.BaseActivity;
import lt.ito.vatconverter.utils.CommonUtils;

/**
 * Created by Jahid on 3/24/2019
 */
public class VatActivity extends BaseActivity implements VatView {
    @Inject
    VatPresenter<VatView, VatInteractor> mPresenter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.spinner_country)
    Spinner mSpinnerCountry;
    @BindView(R.id.radio_group)
    RadioGroup mRadioGroup;
    @BindView(R.id.edit_text_amount)
    EditText mEditTextAmount;
    @BindView(R.id.text_view_result_amount)
    TextView mTextViewResult;

    private ArrayList<String> mCountries = new ArrayList<>();
    private Map<String, Period> mCountryMap = new HashMap<>();

    private Period mSelectedPeriod;
    private Float mSelectedRate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vat);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(this);

        setUp();
    }

    @Override
    protected void setUp() {
        setSupportActionBar(mToolbar);
        mPresenter.onViewPrepared();

        mSpinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSelectedPeriod = mCountryMap.get(mCountries.get(position));
                setRadioButton();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mEditTextAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setResult();
                if(s.length() == 0) {
                    mTextViewResult.setText("");
                }
            }
        });

    }

    private void setRadioButton() {
        mRadioGroup.removeAllViews();
        final Map<Integer, Float> taxTipsMap = new HashMap<>();
        mRadioGroup.setOrientation(LinearLayout.VERTICAL);

        for (Map.Entry<String, Float> entry : mSelectedPeriod.getRates().entrySet()) {
            int id = View.generateViewId();
            taxTipsMap.put(id, entry.getValue());
            RadioButton rbn = new RadioButton(this);
            rbn.setId(id);

            rbn.setText(entry.getValue()+"% ("+entry.getKey()+")");
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
            rbn.setLayoutParams(params);

            mRadioGroup.addView(rbn);

            if (entry.getKey().equalsIgnoreCase("standard")) {
                mRadioGroup.check(id);
                mSelectedRate = entry.getValue();
                setResult();
            }
        }

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mSelectedRate = taxTipsMap.get(checkedId);
                setResult();
            }
        });

    }

    private void setResult() {
        double amount  = mEditTextAmount.getText().length() > 0 ?Double.valueOf(mEditTextAmount.getText().toString()): 0;
        if( amount > 0 && mSelectedRate != null) {
            mTextViewResult.setText(mPresenter.calculatePercentage(amount, mSelectedRate)+"");
        }
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void showData(ArrayList<String> countries, Map<String, Period> countryMap) {
        this.mCountryMap = countryMap;
        this.mCountries = countries;
        CommonUtils.prepareSpinner(this, mSpinnerCountry, mCountries);
    }
}
