package lt.ito.vatconverter.ui.vat;

import io.reactivex.Observable;
import lt.ito.vatconverter.data.network.model.VatRateResponse;
import lt.ito.vatconverter.ui.base.MvpInteractor;

/**
 * Created by Jahid on 3/24/2019
 */
public interface VatInteractor extends MvpInteractor {
    Observable<VatRateResponse> getVatRatesApiCall();
}
