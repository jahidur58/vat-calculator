package lt.ito.vatconverter.ui.vat;

import lt.ito.vatconverter.di.PerActivity;
import lt.ito.vatconverter.ui.base.MvpPresenter;

/**
 * Created by Jahid on 3/24/2019
 */
@PerActivity
public interface VatPresenter <V extends VatView, I extends VatInteractor>
        extends MvpPresenter<V, I> {
    void onViewPrepared();

    double calculatePercentage(double obtained, Float percentage);
}
