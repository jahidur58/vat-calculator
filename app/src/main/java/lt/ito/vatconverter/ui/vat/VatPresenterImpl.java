package lt.ito.vatconverter.ui.vat;

import android.os.Build;
import android.support.annotation.NonNull;
import android.util.Log;

import com.androidnetworking.error.ANError;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import lt.ito.vatconverter.data.network.model.Period;
import lt.ito.vatconverter.data.network.model.Rate;
import lt.ito.vatconverter.data.network.model.VatRateResponse;
import lt.ito.vatconverter.ui.base.BasePresenter;
import lt.ito.vatconverter.utils.rx.SchedulerProvider;

/**
 * Created by Jahid on 3/24/2019
 */
public class VatPresenterImpl <V extends VatView, I extends VatInteractor>
        extends BasePresenter<V, I> implements VatPresenter<V, I> {

    @Inject
    public VatPresenterImpl(I mvpInteractor,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
        getMvpView().showLoading();
        getCompositeDisposable().add(getInteractor()
                .getVatRatesApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<VatRateResponse>() {
                    @Override
                    public void accept(@NonNull VatRateResponse vatRateResponse)
                            throws Exception {
                        if (vatRateResponse != null && vatRateResponse.getRates() != null) {
                            ArrayList<String> countries = new ArrayList<>();
                            Map<String, Period> countryMap = new HashMap<>();
                            for(Rate rate : vatRateResponse.getRates()) {
                                countryMap.put(rate.getName(),Collections.max(rate.getPeriods(), new EffectiveDateComp()));
                                countries.add(rate.getName());
                            }
                           getMvpView().showData(countries, countryMap);
                        }
                        getMvpView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();

                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

    class EffectiveDateComp implements Comparator<Period>{

        @Override
        public int compare(Period e1, Period e2) {
            return e1.getEffectiveFrom().compareTo(e2.getEffectiveFrom());
        }
    }

    @Override
    public double calculatePercentage(double obtained, Float percentage) {
        double num = (obtained *percentage)/100;
        return num + obtained;
    }
}
