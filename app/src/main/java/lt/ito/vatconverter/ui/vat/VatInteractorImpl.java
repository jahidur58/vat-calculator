package lt.ito.vatconverter.ui.vat;

import javax.inject.Inject;

import io.reactivex.Observable;
import lt.ito.vatconverter.data.network.ApiHelper;
import lt.ito.vatconverter.data.network.model.VatRateResponse;
import lt.ito.vatconverter.ui.base.BaseInteractor;

/**
 * Created by Jahid on 3/24/2019
 */
public class VatInteractorImpl extends BaseInteractor implements VatInteractor{

    @Inject
    public VatInteractorImpl(ApiHelper apiHelper) {
        super(apiHelper);
    }

    @Override
    public Observable<VatRateResponse> getVatRatesApiCall() {
        return getApiHelper().doVatRatesApiCall();
    }
}
