package lt.ito.vatconverter.ui.vat;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lt.ito.vatconverter.data.network.model.Period;
import lt.ito.vatconverter.data.network.model.Rate;
import lt.ito.vatconverter.ui.base.MvpView;

/**
 * Created by Jahid on 3/24/2019
 */
public interface VatView extends MvpView {
    void showData(ArrayList<String> countries, Map<String, Period> countryMap);
}
