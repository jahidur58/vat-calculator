package lt.ito.vatconverter.data.network.model;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jahid on 3/20/2019
 */
public class Rate {
    @SerializedName("name")
    private String name;
    @SerializedName("code")
    private String code;
    @SerializedName("country_code")
    private String countryCode;
    @SerializedName("periods")
    private List<Period> periods = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public List<Period> getPeriods() {
        return periods;
    }

    public void setPeriods(List<Period> periods) {
        this.periods = periods;
    }
}
