package lt.ito.vatconverter.data.network.model;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jahid on 3/21/2019
 */
public class VatRateResponse  {
    @SerializedName("details")
    private String details;
    @SerializedName("rates")
    private List<Rate> rates = new ArrayList<>();

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public List<Rate> getRates() {
        return rates;
    }

    public void setRates(List<Rate> rates) {
        this.rates = rates;
    }
}
