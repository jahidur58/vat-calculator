
package lt.ito.vatconverter.data.network;

/**
 * Created by Jahid on 01/02/17.
 */

public final class ApiEndPoint {

    public static final String ENDPOINT_VAT_RATES = "https://jsonvat.com/";


    private ApiEndPoint() {
        // This class is not publicly instantiable
    }

}
