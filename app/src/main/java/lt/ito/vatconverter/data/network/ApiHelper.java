
package lt.ito.vatconverter.data.network;

import javax.inject.Singleton;

import io.reactivex.Observable;
import lt.ito.vatconverter.data.network.model.VatRateResponse;

/**
 * Created by Jahid on 27/01/17.
 */

@Singleton
public interface ApiHelper {

    Observable<VatRateResponse> doVatRatesApiCall();

}
