package lt.ito.vatconverter.data.network.model;


import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jahid on 3/20/2019
 */
public class Period  {
    @SerializedName("effective_from")
    private Date effectiveFrom;
    @SerializedName("rates")
    private Map<String, Float> rates = new HashMap<>();

    public Date getEffectiveFrom() {
        return effectiveFrom;
    }

    public void setEffectiveFrom(Date effectiveFrom) {
        this.effectiveFrom = effectiveFrom;
    }

    public Map<String, Float> getRates() {
        return rates;
    }

    public void setRates(Map<String, Float> rates) {
        this.rates = rates;
    }
}
