/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package lt.ito.vatconverter.data.network;

import com.rx2androidnetworking.Rx2AndroidNetworking;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import lt.ito.vatconverter.data.network.model.VatRateResponse;

/**
 * Created by Jahid on 28/01/17.
 */

@Singleton
public class AppApiHelper implements ApiHelper {
    @Inject
    public AppApiHelper() {
    }


    @Override
    public Observable<VatRateResponse> doVatRatesApiCall() {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_VAT_RATES)
                .build()
                .getObjectObservable(VatRateResponse.class);
    }


}

